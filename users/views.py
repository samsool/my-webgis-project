from django.shortcuts import render
from .forms import LoginForm
from django.contrib.auth import views as auth_views
from django.shortcuts import redirect
from django.contrib import messages
from django.contrib.auth import get_user_model

User = get_user_model()

class LoginView(auth_views.LoginView):
    form_class = LoginForm
    template_name = 'users/login.html'
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect('beranda')
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self,form):
        messages.success(self.request,f"selamat datang {form.cleaned_data['username']}!")
        return super().form_valid(form)

