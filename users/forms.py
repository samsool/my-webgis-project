from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import get_user_model
from django import forms
from django.shortcuts import redirect
from django.contrib import messages
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import get_user_model
from django import forms
from django.shortcuts import redirect
from django.contrib import messages
from django.utils.translation import gettext_lazy as _


class LoginForm(AuthenticationForm):
    username = forms.CharField(label='Username / No Telepon',
                               widget=forms.TextInput(
                               attrs={'class':'form-control',
                                       "autofocus": True,}))
    password = forms.CharField(
        label=_("Password"),
        strip=False,
        widget=forms.PasswordInput(attrs={"autocomplete": "current-password",
                                          'class':'form-control',
                                          "autofocus": True,}),
    )


    error_messages = {
        "invalid_login": _(
            "Terjadi kesalahan. "
            "Username atau password yang anda input salah!"
        ),
        "inactive": _("This account is inactive."),
    }

 

