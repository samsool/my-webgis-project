from django.shortcuts import render
from django.views.generic import DetailView,ListView,CreateView,DeleteView,UpdateView
from django.views import View
from django.contrib import messages
from django.db.models import Q
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
# Create your views here.
from . import models
from .forms import uploadMapForm,uploadWisataForm,uploadPotensiForm
from django.contrib import messages
from django.shortcuts import redirect
from django.views import View
from blog.models import Inbox
from .forms import uploadMapForm
class uploadMap(CreateView,LoginRequiredMixin):
	form_class = uploadMapForm
	template_name = 'webgis/map_upload.html'

	def form_valid(self, form):
		try:
			form.instance.User = self.request.user
			form.save(commit=True)
			messages.success(self.request,'upload map berhasil')
		except Exception as e:
			messages.warning(self.request,f'upload gagal, {e}')
		return redirect('buat-peta')

class buatObjekWisata(CreateView,LoginRequiredMixin):
	form_class = uploadWisataForm
	template_name = 'webgis/wisata_upload.html'

	def form_valid(self, form):
		try:
			form.instance.User = self.request.user
			form.save(commit=True)
			messages.success(self.request,'upload titik wisata berhasil')
		except Exception as e:
			messages.warning(self.request,f'upload wisata gagal, {e}')
		return redirect('buat-wisata')

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['inbox'] = Inbox.objects.all().first()
		return context
class buatObjekPotensi(CreateView,LoginRequiredMixin):
	form_class = uploadPotensiForm
	template_name = 'webgis/buat_potensi.html'

	def form_valid(self, form):
		try:
			form.instance.User = self.request.user
			form.save(commit=True)
			messages.success(self.request,'upload titik potensi berhasil')
		except Exception as e:
			messages.warning(self.request,f'upload potensi gagal, {e}')
		return redirect('buat-potensi')

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['inbox'] = Inbox.objects.all().first()
		return context
def peta(request):
	data = {'Garam': models.map.objects.filter(jenis = 'Garam'),
	 		'Mangrove': models.map.objects.filter(jenis = 'Mangrove'),
	 		'Administrasi': models.map.objects.filter(jenis='Administrasi'),
	 		'wisata':models.wisata.objects.all(),
	 		'potensi':models.potensi.objects.all()}
	data['inbox'] = Inbox.objects.all().first()
	return render(request,'webgis/map.html',data)

class tambah_wisata(CreateView,LoginRequiredMixin):
	pass


class daftar_wisata(LoginRequiredMixin,ListView):
	model = models.wisata
	template_name = 'webgis/daftar_wisata.html'
	context_object_name = 'objects'
	paginate_by = 5

	def get_queryset(self,*args,**kwargs):
		query = self.request.GET.get('cari')
		if query:
			return models.wisata.objects.filter(Q(nama__icontains = query))
		return models.wisata.objects.all().order_by('nama')
	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['inbox'] = Inbox.objects.all().first()
		return context

class daftar_geojson(LoginRequiredMixin,ListView):
	model = models.map
	template_name = 'webgis/daftar_geojson.html'
	context_object_name = 'objects'
	paginate_by = 5

	def get_queryset(self,*args,**kwargs):
		query = self.request.GET.get('cari')
		if query:
			return models.map.objects.filter(Q(nama__icontains = query))
		return models.map.objects.all().order_by('nama')
	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['inbox'] = Inbox.objects.all().first()
		return context

class daftar_potensi(LoginRequiredMixin,ListView):
	model = models.potensi
	template_name = 'webgis/daftar_potensi.html'
	context_object_name = 'objects'
	paginate_by = 5

	def get_queryset(self,*args,**kwargs):
		query = self.request.GET.get('cari')
		if query:
			return models.potensi.objects.filter(Q(nama__icontains = query))
		return models.potensi.objects.all().order_by('nama')
	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['inbox'] = Inbox.objects.all().first()
		return context

class HapusWisata(LoginRequiredMixin, DeleteView):
    model = models.wisata
    success_url = 'daftar-wisata'
    template_name = 'webgis/hapus-wisata.html'
    context_object_name = 'objek'


    def form_valid(self, form):
        try:
            self.object = self.get_object()
            self.object.delete()
            messages.success(self.request,f'Wisata berhasil dihapus')
            return redirect('daftar-wisata')
        except Exception as e:
            messages.warning(self.request,f'Ada kesalahan {e}')
            return redirect('daftar-wisata')
class HapusPotensi(LoginRequiredMixin, DeleteView):
    model = models.potensi
    success_url = 'daftar-potensi'
    template_name = 'webgis/hapus_potensi.html'
    context_object_name = 'objek'


    def form_valid(self, form):
        try:
            self.object = self.get_object()
            self.object.delete()
            messages.success(self.request,f'Potensi berhasil dihapus')
            return redirect('daftar-potensi')
        except Exception as e:
            messages.warning(self.request,f'Ada kesalahan {e}')
            return redirect('daftar-potensi')
class HapusMap(LoginRequiredMixin, DeleteView):
    model = models.map
    success_url = 'daftar-geojson'
    template_name = 'webgis/hapus_map.html'
    context_object_name = 'objek'


    def form_valid(self, form):
        try:
            self.object = self.get_object()
            self.object.delete()
            messages.success(self.request,f'geojson anda berhasil dihapus')
            return redirect('daftar-geojson')
        except Exception as e:
            messages.warning(self.request,f'Ada kesalahan {e}')
            return redirect('daftar-geojson')
class EditWisata(LoginRequiredMixin, UserPassesTestMixin,UpdateView):
    form_class = uploadWisataForm
    template_name = 'webgis/wisata_upload.html'
    model = models.wisata
    def test_func(self):
        post = self.get_object()
        if self.request.user == post.User:
            return True
        return False

    def form_valid(self, form): 
        try:
            form.save(commit=True)
            messages.success(self.request,"wisata anda berhasil diubah !")
            return redirect('daftar-wisata')
        except Exception as e:
            messages.warning(self.request,f'Ada kesalahan {e}')
            return redirect('daftar-wisata')

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        context['gambarnya'] = models.wisata.objects.filter(pk=self.kwargs['pk']).first()
        context['inbox'] = Inbox.objects.all().first()
        return context
class EditPotensi(LoginRequiredMixin, UserPassesTestMixin,UpdateView):
    form_class = uploadPotensiForm
    template_name = 'webgis/buat_potensi.html'
    model = models.potensi
    def test_func(self):
        post = self.get_object()
        if self.request.user == post.User:
            return True
        return False

    def form_valid(self, form): 
        try:
            form.save(commit=True)
            messages.success(self.request,"Objek potensi anda berhasil diubah !")
            return redirect('daftar-potensi')
        except Exception as e:
            messages.warning(self.request,f'Ada kesalahan {e}')
            return redirect('daftar-potensi')

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        context['gambarnya'] = models.potensi.objects.filter(pk=self.kwargs['pk']).first()
        context['inbox'] = Inbox.objects.all().first()
        return context


class EditMap(LoginRequiredMixin, UserPassesTestMixin,UpdateView):
    form_class = uploadMapForm
    template_name = 'webgis/map_upload.html'
    model = models.map
    def test_func(self):
        post = self.get_object()
        if self.request.user == post.User:
            return True
        return False

    def form_valid(self, form): 
        try:
            form.save(commit=True)
            messages.success(self.request,"wisata anda berhasil diubah !")
            return redirect('daftar-geojson')
        except Exception as e:
            messages.warning(self.request,f'Ada kesalahan {e}')
            return redirect('daftar-geojson')

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        context['gambarnya'] = models.map.objects.filter(pk=self.kwargs['pk']).first()
        context['inbox'] = Inbox.objects.all().first()
        return context