from django.contrib import admin
from . import models
# Register your models here.

admin.site.register(models.potensi)
admin.site.register(models.wisata)
admin.site.register(models.map)