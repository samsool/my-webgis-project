from django.db import models
from django.contrib.auth import get_user_model
import datetime as dt
from PIL import Image
from io import BytesIO
# Create your models here.
from django.core.files.storage import default_storage
User = get_user_model()


class wisata(models.Model):
	User = models.ForeignKey(User,on_delete=models.CASCADE)
	foto = models.ImageField(default = 'default.png',upload_to='wisata',null=True,blank=True)
	nama = models.CharField(max_length=100)
	deskripsi = models.TextField()
	latitude = models.FloatField(default = 0)
	longtitude = models.FloatField(default = 0)


	def __str__ (self):
		return self.nama

	def save(self):
		super().save()
		memfile = BytesIO()

		img = Image.open(self.foto)

		if img.height > 250 or img.width > 250:
			output_size = (250, 250)
			img.thumbnail(output_size)
			img.save(memfile,'PNG',quality=100)
			default_storage.save(self.foto.name, memfile)
			memfile.close()
			img.close()

class potensi(models.Model):
	User = models.ForeignKey(User,on_delete=models.CASCADE)
	foto = models.ImageField(default = 'default.png',upload_to='wisata',null=True,blank=True)
	nama = models.CharField(max_length=100)
	deskripsi = models.TextField()
	latitude = models.FloatField(default = 0)
	longtitude = models.FloatField(default = 0)


	def __str__ (self):
		return self.nama

	def save(self):
		super().save()
		memfile = BytesIO()

		img = Image.open(self.foto)

		if img.height > 250 or img.width > 250:
			output_size = (250, 250)
			img.thumbnail(output_size)
			img.save(memfile,'PNG',quality=100)
			default_storage.save(self.foto.name, memfile)
			memfile.close()
			img.close()

kategori = (
    ('Mangrove', 'Mangrove'),
    ('Garam', 'Garam'),
    ('Administrasi', 'Administrasi'),
    ('Akresi', 'Akresi'),
    ('Transportasi', 'Transportasi'),
)


class map(models.Model): 
	nama = models.CharField(max_length=100)
	jenis = models.CharField(max_length=50,choices=kategori)
	warna = models.CharField(max_length=20,default='#0394fc')
	warna_dalam = models.CharField(max_length=20,default='#0394fc')
	opacity = models.FloatField(default=1)
	fillOpacity = models.FloatField(default=1)
	User = models.ForeignKey(User,on_delete=models.CASCADE)
	geojson = models.FileField(upload_to='geojson',null=True,blank=True)
	deskripsi = models.TextField(null=True,blank=True)
	latitude = models.FloatField(default = 0)
	longtitude = models.FloatField(default = 0)

	def __str__ (self):
		return self.nama



