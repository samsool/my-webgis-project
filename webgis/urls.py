"""sisirpatiwebapp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from django.conf import settings
from django.conf.urls.static import static
from . import views
urlpatterns = [
    path('peta/',views.peta,name='peta'),
    path('buat_peta/',views.uploadMap.as_view(),name='buat-peta'),
    path('hapus_peta/<pk>',views.HapusMap.as_view(),name='hapus-peta'),
    path('edit_peta/<pk>',views.EditMap.as_view(),name='edit-peta'),
    path('buat_wisata/',views.buatObjekWisata.as_view(),name='buat-wisata'),
    path('daftar_wisata/',views.daftar_wisata.as_view(),name='daftar-wisata'),
    path('daftar_geojson/',views.daftar_geojson.as_view(),name='daftar-geojson'),
    path('hapus_wisata/<pk>',views.HapusWisata.as_view(),name='hapus-wisata'),
    path('buat_potensi/',views.buatObjekPotensi.as_view(),name='buat-potensi'),
    path('edit_wisata/<pk>',views.EditWisata.as_view(),name='edit-potensi'),
    path('daftar_potensi/',views.daftar_potensi.as_view(),name='daftar-potensi'),
    path('hapus_potensi/<pk>',views.HapusPotensi.as_view(),name='hapus-potensi'),
    path('edit_potensi/<pk>',views.EditPotensi.as_view(),name='edit-potensi'),
]

if settings.DEBUG:
	urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)