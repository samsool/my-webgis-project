# Generated by Django 4.0.4 on 2022-06-08 12:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webgis', '0013_alter_wisata_latitude_alter_wisata_longtitude'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='potensi',
            name='tanggal_berakhir',
        ),
        migrations.RemoveField(
            model_name='potensi',
            name='tanggal_mulai',
        ),
        migrations.AlterField(
            model_name='potensi',
            name='latitude',
            field=models.FloatField(default=0),
        ),
        migrations.AlterField(
            model_name='potensi',
            name='longtitude',
            field=models.FloatField(default=0),
        ),
    ]
