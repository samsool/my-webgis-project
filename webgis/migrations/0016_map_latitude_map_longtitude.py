# Generated by Django 4.0.4 on 2022-06-09 19:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webgis', '0015_remove_wisata_tanggal_berakhir_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='map',
            name='latitude',
            field=models.FloatField(default=0),
        ),
        migrations.AddField(
            model_name='map',
            name='longtitude',
            field=models.FloatField(default=0),
        ),
    ]
