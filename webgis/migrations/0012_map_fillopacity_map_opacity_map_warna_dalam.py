# Generated by Django 4.0.4 on 2022-05-23 01:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webgis', '0011_alter_map_warna'),
    ]

    operations = [
        migrations.AddField(
            model_name='map',
            name='fillOpacity',
            field=models.IntegerField(default=1),
        ),
        migrations.AddField(
            model_name='map',
            name='opacity',
            field=models.IntegerField(default=1),
        ),
        migrations.AddField(
            model_name='map',
            name='warna_dalam',
            field=models.CharField(default='#0394fc', max_length=20),
        ),
    ]
