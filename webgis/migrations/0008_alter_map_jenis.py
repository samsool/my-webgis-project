# Generated by Django 4.0.4 on 2022-05-14 02:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webgis', '0007_delete_uploadmap'),
    ]

    operations = [
        migrations.AlterField(
            model_name='map',
            name='jenis',
            field=models.CharField(choices=[('Mangrove', 'Mangrove'), ('Garam', 'Garam'), ('Administrasi', 'Administrasi'), ('Akresi', 'Akresi'), ('Transportasi', 'Transportasi')], max_length=50),
        ),
    ]
