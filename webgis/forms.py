from django.forms import ModelForm, TextInput, EmailInput, Textarea,MultipleChoiceField, FileInput
from . import models

kategori = (
    ('Mangrove', 'Mangrove'),
    ('Garam', 'Garam'),
    ('Administrasi', 'Administrasi'),
    ('Akresi', 'Akresi'),
    ('Transportasi', 'Transportasi'),
)

class uploadMapForm(ModelForm):
	class Meta:
		model = models.map
		fields= ['nama','warna','warna_dalam','opacity','geojson','fillOpacity','jenis','latitude','longtitude']

		widgets = {
		'nama': TextInput(attrs={'class':'form-control','id':'judul',}),
		#'geojson': FileInput(attrs={'onchange':'showPreview(event)','class':'form-control-file'})


        }
class uploadWisataForm(ModelForm):
	class Meta:
		model = models.wisata
		fields= ['foto','nama','deskripsi','latitude','longtitude']

		widgets = {
		'nama': TextInput(attrs={'class':'form-control','id':'judul',}),
		#'geojson': FileInput(attrs={'onchange':'showPreview(event)','class':'form-control-file'})


        }
class uploadPotensiForm(ModelForm):
	class Meta:
		model = models.potensi
		fields= ['foto','nama','deskripsi','latitude','longtitude']

		widgets = {
		'nama': TextInput(attrs={'class':'form-control','id':'judul',}),
		#'geojson': FileInput(attrs={'onchange':'showPreview(event)','class':'form-control-file'})


        }