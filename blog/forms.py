from django.forms import ModelForm, TextInput, EmailInput, Textarea,FileInput
from .models import berita,Kategori



class BuatBeritaForm(ModelForm):
    class Meta:
        model = berita
        fields = ['judul','link','tanggal','headline','thumbnail','isi']

        widgets = {
            'judul': TextInput(attrs={'class': 'form-control id_judul','id':'judul',}),
            'headline':TextInput(attrs={'class': 'form-control id_headline','id':'headline'}),
        


        }




class BuatKategoriForm(ModelForm):
    class Meta:
        model = Kategori
        fields = ['gambar','nama','deskripsi','link']

        widgets = {
            'nama': TextInput(attrs={'class': 'form-control id_judul','id':'judul',}),
            'deskripsi':Textarea(attrs={'class': 'form-control','id':'kategori','rows=':'3'}),
            #'gambar': FileInput(attrs={'class':'nama','onchange':'showPreview(event)','id':'file-ip-1','accept':'image/*'})


        }
