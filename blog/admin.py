from django.contrib import admin
from .models import pengaduan, berita,Kategori,Inbox
from django.db import models
# Register your models here.
"""
class TulisBerita(admin.ModelAdmin):
	formfield_overrides = {
		models.TextField:{'widget': TinyMCE()}
	}
"""
admin.site.register(pengaduan)
admin.site.register(berita)
admin.site.register(Kategori)
admin.site.register(Inbox)