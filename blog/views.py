from django.shortcuts import render,redirect
from django.http import HttpResponse
from . models import berita,pengaduan,Kategori,Inbox
from django.views.generic import DetailView,ListView,CreateView,DeleteView,UpdateView
from django.views import View
from django.contrib import messages
from django.db.models import Q
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
import math
from .forms import BuatBeritaForm,BuatKategoriForm
import datetime as dt
# Create your views here.
class HapusPengaduan(LoginRequiredMixin, DeleteView):
    model = pengaduan
    success_url = '/pesan/'
    template_name = 'blog/hapus-pengaduan.html'
    context_object_name = 'objek'


    def form_valid(self, form):
        try:
            self.object = self.get_object()
            self.object.delete()
            messages.success(self.request,f'pengaduan berhasil dihapus')
            return redirect(self.get_success_url())
        except Exception as e:
            messages.warning(self.request,f'Ada kesalahan {e}')
            return redirect(self.get_success_url())

class HapusKategori(LoginRequiredMixin, DeleteView):
    model = Kategori
    success_url = 'kelola-kategori'
    template_name = 'blog/hapus-kategori.html'
    context_object_name = 'objek'


    def form_valid(self, form):
        try:
            self.object = self.get_object()
            self.object.delete()
            messages.success(self.request,f'Kategori berhasil dihapus')
            return redirect(self.get_success_url())
        except Exception as e:
            messages.warning(self.request,f'Ada kesalahan {e}')
            return redirect(self.get_success_url())

class berandaUtama(ListView):
	template_name = 'blog/beranda.html'
	model = berita
	context_object_name = 'berita'
	def get_context_data(self, **kwargs):
		# Call the base implementation first to get a context
		context = super().get_context_data(**kwargs)
        # Add in a QuerySet of all the books
		context['berita'] = berita.objects.all().order_by('-tanggal')[:10]
		context['inbox'] = Inbox.objects.all().first()
		return context

class CariBerita(View):
	def get(self,request,*args,**kwargs):
		item = self.request.GET.get('cari')
		context = dict()
		context['inbox'] = Inbox.objects.all().first()
		if item:
			query = berita.objects.filter(Q(judul__icontains=item)| Q(isi__icontains=item) |Q(headline__icontains = item)).distinct()
			halamanAktif = int(self.request.GET.get('halaman')) if int(self.request.GET.get('halaman')) else 1
			jumlahDataPerHalaman = 5
			jumlahDataAktif = math.ceil(len(query)/jumlahDataPerHalaman)
			awalData =( jumlahDataPerHalaman * halamanAktif ) - jumlahDataPerHalaman
			sebelumnya =  halamanAktif -1 if halamanAktif-1 != 0 else jumlahDataAktif
			berikutnya =  halamanAktif + 1 if halamanAktif+1 < jumlahDataAktif  else 0
			queryPush = None
			queryPush = query[awalData:awalData+jumlahDataPerHalaman] 
			print(queryPush)
			context = {'item':item,
					'Queries':range(1,jumlahDataAktif+1),
					'dataDitemukan' : len(query),
					'beritaCari' :queryPush,
					'sebelumnya':sebelumnya,
					'berikutnya':berikutnya,
					'akhir':jumlahDataAktif,
					'sekarang':halamanAktif}
			context['berita'] = berita.objects.all().order_by('-tanggal')[:3]
		return render(self.request,'blog/pencarian.html',context)

	def get_context_data(self, **kwargs):
		# Call the base implementation first to get a context

		item = self.request.GET.get('cari',False)
		context = super().get_context_data(**kwargs)
		# Add in a QuerySet of all the books
		context['item'] = item

		context['berita'] = berita.objects.filter(Q(judul__icontains=item)| Q(isi__icontains=item) |Q(kategori__icontains = item)|Q(headline__icontains = item)).distinct()
		if not item:
			context['berita'] =  []
		return context




class pengaduanView(View):
	def get(self,request,*args,**kwargs):
		
		context = {'berita':berita.objects.all().order_by('-tanggal')[:3]}
		context['inbox'] = Inbox.objects.all().first()
		return render(self.request,'blog/pengaduan.html',context)
	def post(self,request,*args,**kwargs):
		if self.request.method == "POST":
			nama1 = self.request.POST['id_nama']
			alamat = self.request.POST['id_alamat']
			noHP = self.request.POST['id_no_hp']
			isi = self.request.POST['id_isi']
			foto = self.request.FILES['foto']
			baru = pengaduan(nama=nama1,alamat=alamat,no_hp=noHP,isi=isi,foto=foto)
			baru.save()
			pesan = Inbox.objects.all().first()
			pesan.pesan_masuk += 1
			pesan.save()
			messages.success(self.request,"Pesan anda berhasil terkirim")
		return redirect('ngadu')

		

class beritaView(ListView):
	template_name = 'blog/berita.html'
	model = Kategori
	context_object_name = 'kategorilist'
	def get_queryset(self,**kwargs):
		item = self.request.GET.get('cari')
		if item:
			return Kategori.objects.filter(Q(nama__icontains=item)).distinct()
		return Kategori.objects.all().order_by('nama')
	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['berita'] = berita.objects.all().order_by('-tanggal')[:5]
		context['inbox'] = Inbox.objects.all().first()
		#context['diupdate'] = dt.datetime.now().hour
		return context


class KategoriView(ListView):
	model = berita
	template_name = 'blog/kategori.html'
	context_object_name = 'berita'
	paginate_by = 5

	def get_queryset(self,*args,**kwargs):
		item = self.request.GET.get('cari_berita')
		if item:
			return berita.objects.filter(Q(judul__icontains=item)| Q(isi__icontains=item)|Q(headline__icontains = item),kategori__link=self.kwargs['slug']).distinct()
		return berita.objects.filter(kategori__link=self.kwargs['slug'])
	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		self.request.session['kategori'] = self.kwargs['slug']
		context['data'] = Kategori.objects.filter(link = self.kwargs['slug']).first()
		context['inbox'] = Inbox.objects.all().first()
		return context

class KelolaKategori(ListView):
	model = Kategori
	template_name = 'blog/list_kategori.html'
	context_object_name = 'objects'
	paginate_by = 5

	def get_queryset(self,*args,**kwargs):
		item = self.request.GET.get('cari')
		if item:
			return Kategori.objects.filter(Q(nama__icontains=item)| Q(deskripsi__icontains=item)).distinct()
		return Kategori.objects.all().order_by('nama')
	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['berita'] = berita.objects.all().order_by('-tanggal')[:5]
		context['inbox'] = Inbox.objects.all().first()
		#context['diupdate'] = dt.datetime.now().hour
		return context

class KelolaBerita(ListView):
	model = berita
	template_name = 'blog/list_berita.html'
	context_object_name = 'objects'
	paginate_by = 5

	def get_queryset(self,*args,**kwargs):
		item = self.request.GET.get('cari')
		if item:
			return berita.objects.filter(Q(judul__icontains=item)| Q(isi__icontains=item)|Q(headline__icontains = item),kategori__link=self.kwargs['slug']).distinct()
		return berita.objects.filter(kategori__link=self.kwargs['slug']).order_by('-tanggal')[:5]

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['berita'] = berita.objects.all().order_by('-tanggal')[:5]
		context['inbox'] = Inbox.objects.all().first()
		#context['diupdate'] = dt.datetime.now().hour
		return context

 


class DetailBerita(View):
    def get(self,request,*args,**kwargs):
        # Call the base implementation first to get a context
       	renderhtml = 'blog/notfound.html'
       	context = dict()
       	context['inbox'] = Inbox.objects.all().first()
        target1 = berita.objects.filter(link=self.kwargs['other_slug'],kategori__link= self.kwargs['slug']).first()
        if target1:
            target1.dilihat += 1
            target1.save()
            context = dict()
            context['targetBerita'] = target1
            allNews = list(berita.objects.filter(kategori__link= self.kwargs['slug']))
            currentNews = allNews.index(context['targetBerita'])
            context['berikutnya'] = berita.objects.filter(kategori__link= self.kwargs['slug'])[0]
            context['sebelumnya'] = berita.objects.filter(kategori__link= self.kwargs['slug'])[len(allNews)-1]
            if currentNews + 1 != len(allNews):
                context['berikutnya'] =  berita.objects.filter(kategori__link= self.kwargs['slug'])[currentNews + 1]
            if currentNews - 1 != -1:
                context['sebelumnya']=  berita.objects.filter(kategori__link= self.kwargs['slug'])[currentNews - 1]
            context['berita'] = berita.objects.filter(kategori__link= self.kwargs['slug'])
            renderhtml = 'blog/detail-berita.html'

        return render(self.request,renderhtml,context)

class BuatBerita(LoginRequiredMixin,CreateView):
	form_class = BuatBeritaForm
	template_name = 'blog/buat-berita.html'

	def form_valid(self, form): 
		try:
			form.save(commit=False)
			form.instance.user = self.request.user
			kategori = Kategori.objects.get(link = self.request.session['kategori'])
			form.instance.kategori = kategori
			form.save(commit=True)
			messages.success(self.request,"berita anda berhasil diposting !")
			return redirect('buatBerita', kategori)
		except Exception as e:
			messages.warning(self.request,f'Ada kesalahan {e}')
			return redirect('buatBerita', kategori)
	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['inbox'] = Inbox.objects.all().first()
		return context


class BuatKategori(LoginRequiredMixin,CreateView):
	form_class = BuatKategoriForm
	template_name = 'blog/buat-kategori.html'

	def form_valid(self, form): 
		try:
			form.save(commit=False)
			form.instance.user = self.request.user
			form.save(commit=True)
			messages.success(self.request,"kategori baru anda berhasil diposting !")
			return redirect('kategori-baru')
		except Exception as e:
			messages.warning(self.request,f'Ada kesalahan {e}')
			return redirect('kategori-baru')
	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['inbox'] = Inbox.objects.all().first()
		return context
"""
class BuatKategori(LoginRequiredMixin,CreateView):
	form_class = kategoriForm
	template_name = 'blog/buat-kategori.html'
	

	def form_valid(self, form): 
		try:
			form.save(commit=False)
			form.instance.user = self.request.user
			form.save(commit=True)
			messages.success(self.request,"berita anda berhasil diposting !")
			return redirect('buatBerita')
		except Exception as e:
			messages.warning(self.request,f'Ada kesalahan {e}')
			return redirect('buatBerita')
"""
class EditBerita(LoginRequiredMixin, UserPassesTestMixin,UpdateView):
    form_class = BuatBeritaForm
    template_name = 'blog/buat-berita.html'
    model = berita

    def test_func(self):
        post = self.get_object()
        if self.request.user == post.user:
            return True
        return False

    def form_valid(self, form): 
        try:
            form.save(commit=True)
            messages.success(self.request,"berita anda berhasil diubah !")
            return redirect('editBerita',slug=self.kwargs['slug'],pk=form.instance.pk)
        except Exception as e:
            messages.warning(self.request,f'Ada kesalahan {e}')
            return redirect('editBerita',slug=self.kwargs['slug'],pk=form.instance.pk)

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        context['berita'] = berita.objects.all().order_by('-tanggal')[:3]
        context['inbox'] = Inbox.objects.all().first()
        return context
  
class EditKategori(LoginRequiredMixin,UpdateView):
    form_class = BuatKategoriForm
    template_name = 'blog/buat-kategori.html'
    model = Kategori



    def form_valid(self, form): 
        try:
            form.save(commit=True)
            messages.success(self.request,"kategori anda berhasil diubah !")
            return redirect('edit-kategori',pk=self.kwargs['pk'])
        except Exception as e:
            messages.warning(self.request,f'Ada kesalahan {e}')
            return redirect('edit-kategori',pk=self.kwargs['pk'])

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        context['gambarnya'] = Kategori.objects.filter(pk = self.kwargs['pk']).first()
        context['berita'] = berita.objects.all().order_by('-tanggal')[:3]
        context['inbox'] = Inbox.objects.all().first()
        return context
  
class HapusBerita(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = berita
    success_url = 'kelola-berita'
    template_name = 'blog/hapus-berita.html'
    context_object_name = 'objek'

    def test_func(self):
        post = self.get_object()
        if self.request.user == post.user:
            return True
        return False

    def form_valid(self, form):
        try:
            self.object = self.get_object()
            self.object.delete()
            messages.success(self.request,f'Berita berhasil dihapus')
            return redirect('kelola-berita',self.request.session['kategori'])
        except Exception as e:
            messages.warning(self.request,f'Ada kesalahan {e}')
            return redirect('kelola-berita',self.request.session['kategori'])



class LihatPengaduan(LoginRequiredMixin,ListView):
	model = pengaduan
	template_name = 'blog/pesan-masuk.html'
	context_object_name = 'objects'
	paginate_by = 5

	def get_queryset(self,*args,**kwargs):
		return pengaduan.objects.all().order_by('-tanggal')
	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['inbox'] = Inbox.objects.all().first()
		if context['inbox']:
			context['inbox'].pesan_masuk = 0;
			context['inbox'].save()
		return context

class LaporanDetail(LoginRequiredMixin,DetailView):
    model = pengaduan
    template_name = 'blog/detail-pengaduan.html'
    context_object_name = 'object'
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['inbox'] = Inbox.objects.all().first()
        data = pengaduan.objects.get(pk=self.kwargs['pk'])
        data.status = 'sudah dibaca'
        data.save()
        return context

class EditBerita(LoginRequiredMixin, UserPassesTestMixin,UpdateView):
    form_class = BuatBeritaForm
    template_name = 'blog/buat-berita.html'
    model = berita

    def test_func(self):
        post = self.get_object()
        if self.request.user == post.user:
            return True
        return False

    def form_valid(self, form): 
        try:
            form.save(commit=True)
            messages.success(self.request,"berita anda berhasil diubah !")
            return redirect('editBerita',slug=self.kwargs['slug'],pk=form.instance.pk)
        except Exception as e:
            messages.warning(self.request,f'Ada kesalahan {e}')
            return redirect('editBerita',slug=self.kwargs['slug'],pk=form.instance.pk)

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        #context['berita'] = berita.objects.all().order_by('-tanggal')[:3]
        context['gambarnya'] = berita.objects.filter(pk = self.kwargs['pk']).first()
        context['inbox'] = Inbox.objects.all().first()
        return context
