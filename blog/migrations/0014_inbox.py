# Generated by Django 4.0.4 on 2022-06-05 08:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0013_berita_dilihat'),
    ]

    operations = [
        migrations.CreateModel(
            name='Inbox',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('pesan_masuk', models.IntegerField(default=0)),
            ],
        ),
    ]
