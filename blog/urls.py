"""sisirpatiwebapp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from . import views

urlpatterns = [
    path('isi_pengaduan/',views.pengaduanView.as_view(),name='ngadu'),
    path('',views.berandaUtama.as_view(),name='beranda'),
    path('buat_kategori_berita/',views.BuatKategori.as_view(),name='kategori-baru'),
    path('berita/',views.beritaView.as_view(),name='berita'),
    path('berita/<slug>',views.KategoriView.as_view(),name='kategorilist'),
    path('berita/<slug>/<other_slug>',views.DetailBerita.as_view(),name='detailBerita'),
    path('berita/<slug>/baru/buat_berita',views.BuatBerita.as_view(),name='buatBerita'),
    path('hapus_berita/<int:pk>',views.HapusBerita.as_view(),name='hapusBerita'),
    path('cari_berita/',views.CariBerita.as_view(),name='cariBerita'),
    path('berita/edit/<slug>/<pk>',views.EditBerita.as_view(),name='editBerita'),
    path('pesan/',views.LihatPengaduan.as_view(),name='lihatPesan'),
    path('detail_laporan/<pk>',views.LaporanDetail.as_view(),name='lihatLaporan'),
    path('hapus_laporan/<pk>',views.HapusPengaduan.as_view(),name='hapusLaporan'),
    path('kelola_kategori/',views.KelolaKategori.as_view(),name='kelola-kategori'),
    path('edit_kategori/<pk>',views.EditKategori.as_view(),name='edit-kategori'),
    path('hapus_kategori/<pk>',views.HapusKategori.as_view(),name='hapus-kategori'),
    path('berita/<slug>/edit/kelola_berita',views.KelolaBerita.as_view(),name='kelola-berita'),
]
