from django.db import models
from django.contrib.auth import get_user_model
from django.utils import timezone
from ckeditor_uploader.fields import RichTextUploadingField
from PIL import Image
import datetime as dt
import random
from django.core.files.storage import default_storage
from io import BytesIO

# Create your models here.

class pengaduan(models.Model):
	nama = models.CharField(max_length=50)
	alamat = models.CharField(max_length=100)
	no_hp = models.CharField(max_length=12)
	tanggal = models.DateTimeField('tanggal berita',default = dt.datetime.now)
	isi = models.TextField()
	foto = models.ImageField(default='default.png',upload_to='gambar_laporan',null=True,blank=True)
	status = models.CharField(max_length=50,default='belum dibaca')

	def __str__(self):
		return self.nama

	def delete(self):
		if self.foto.name != "default.png":
			self.foto.delete(save=False)
		return super().delete()

	def save(self):
		super().save()
		memfile = BytesIO()

		img = Image.open(self.foto)

		if img.height > 500 or img.width > 500:
			output_size = (500, 500)
			img.thumbnail(output_size)
			img.save(memfile,'PNG',quality=100)
			default_storage.save(self.foto.name, memfile)
			memfile.close()
			img.close()


User = get_user_model()

class Kategori(models.Model):
	gambar = models.ImageField(default='default.png',upload_to='gambar_kategori',null=True,blank=True)
	nama = models.CharField(max_length=100)
	deskripsi = models.TextField()
	link = models.CharField(max_length=300,null=True,blank=True)

	def __str__(self):
		return self.nama
	def save(self):
		temp = self.nama.replace(" ","_")
		self.link = temp.lower()
		super().save()
		memfile = BytesIO()

		img = Image.open(self.gambar)

		if img.height > 300 or img.width > 300:
			output_size = (300, 300)
			img.thumbnail(output_size)
			img.save(memfile,'PNG',quality=100)
			default_storage.save(self.gambar.name, memfile)
			memfile.close()
			img.close()
	def delete(self):
		if self.gambar.name != "default.png":
			self.gambar.delete(save=False)
		return super().delete()

class berita(models.Model):
	user = models.ForeignKey(User,on_delete = models.CASCADE)
	tanggal = models.DateTimeField('tanggal berita',default = dt.datetime.now)
	judul = models.CharField(max_length=100,null=True,blank=True)
	kategori = models.ForeignKey(Kategori,on_delete=models.CASCADE)
	link = models.CharField(max_length=300,null=True,blank=True)
	headline = models.CharField(max_length=100,null=True,blank=True)
	isi = RichTextUploadingField()
	dilihat = models.IntegerField(default=0,blank=True)
	thumbnail = models.ImageField(default='default.png',upload_to='gambar_berita',null=True,blank=True)
	def __str__(self):
		return self.judul
	def save(self):
		temp = self.judul.replace(" ","_")
		self.link = temp.lower()
		super().save()
		memfile = BytesIO()

		img = Image.open(self.thumbnail)

		if img.height > 500 or img.width > 600:
			output_size = (600, 500)
			img.thumbnail(output_size)
			img.save(memfile,'PNG',quality=100)
			default_storage.save(self.thumbnail.name, memfile)
			memfile.close()
			img.close()

	def delete(self):
		if self.thumbnail.name != "default.png":
			self.thumbnail.delete(save=False)
		return super().delete()

class Inbox(models.Model):
	pesan_masuk = models.IntegerField(default=0)


